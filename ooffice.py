#!/usr/bin/python
import os, sys, uno, tempfile, json
from com.sun.star.beans import PropertyValue
from com.sun.star.io import XOutputStream
from unohelper import Base, systemPathToFileUrl

def main(argv):
	# Get the parameters from stdin
	args = json.loads(argv[1])
	convertor = odtTemplateConvertor(args['template_url'])
	convertor.replace(args['replace'])
	convertor.pdfToStdOut()
	
class OutputStream( Base, XOutputStream ):
	def __init__( self ):
    		self.closed = 0
	def closeOutput(self):
		self.closed = 1
	def writeBytes( self, seq ):
        	sys.stdout.write( seq.value )
	def flush( self ):
        	pass

class odtTemplateConvertor:
	def __init__(self, templateUrl):
		self._connectToOpenOffice('localhost', 2002)
		self.document = self.desktop.loadComponentFromURL(templateUrl ,"_blank", 0, ())
	def __del__(self):
		self.document.dispose()
		
	def _connectToOpenOffice(self, host, port):
		local = uno.getComponentContext()
		resolver = local.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", local)
		# Connect to socket
		context = resolver.resolve("uno:socket,host=" + host + ",port=" + str(port) + ";urp;StarOffice.ComponentContext")
		self.desktop = context.ServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", context)
	
	def replace(self, replacements):
		for find in replacements:
			replace = replacements[find]
			replaceobj = self.document.createReplaceDescriptor()
			replaceobj.SearchString = '{{{' + find + '}}}'
			replaceobj.ReplaceString = replace
			self.document.replaceAll(replaceobj)
	
	def pdfToStdOut(self):
		outProps = (
		  PropertyValue( "FilterName" , 0, "writer_pdf_Export" , 0 ),
                  PropertyValue( "Overwrite" , 0, True , 0 ),
                  PropertyValue( "OutputStream", 0, OutputStream(), 0)
                )
		self.document.storeToURL("private:stream",outProps)

if __name__ == "__main__":
	    sys.exit(main(sys.argv))
