OpenOffice template to pdf converter
====================================

This set of scripts converts an odt file to a pdf, while replacing placeholder tokens.

Starting the OpenOffice server
------------------------------
libreoffice --accept="socket,host=localhost,port=2002;urp;StarOffice.ServiceManager" --norestore --nofirstwizard --nologo --headless

Examples
--------
Direct call to the python script
>  ./ooffice.py "$(< input.json)" > result.pdf

If you place the collection of scripts in a docroot, you can fetch the pdf by visiting index.php
